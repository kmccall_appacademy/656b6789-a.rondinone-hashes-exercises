# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split
  word_length_hash = {}
  words.each { |word| word_length_hash[word] = word.length }
  word_length_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  result = ''
  max = 0
  hash.each do |k, v|
    if v > max
      max = v
      result = k
    end
  end
  result
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |key, value| older[key] = value }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count = Hash.new(0)
  word.each_char do |letter|
    letter_count[letter] += 1
  end
  letter_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  result = []
  uniq_hash = Hash.new(0)
  arr.each { |ele| uniq_hash[ele] += 1}
  uniq_hash.each { |key, value| result << key}
  result
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_hash = {
    even: 0,
    odd: 0
  }
  numbers.each do |num|
    if num % 2 == 0
      even_odd_hash[:even] += 1
    elsif  num % 2 == 1
      even_odd_hash[:odd] += 1
    end
  end
  even_odd_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = 'aeiou'
  vowel_count = Hash.new(0)

  string.each_char do |letter|
    if vowels.include?(letter)
    vowel_count[letter] += 1
    end
  end

  vowel_arr = vowel_count.sort_by {|key, value| value}
  vowel_arr[-1][0]

end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)

  winter_birthdays = {}

  students.each do |student, month|
    if month >= 7
      winter_birthdays[student] = month
    end
  end
  winter_students = winter_birthdays.keys

  combos = []
  (0...(winter_students.length - 1)).each do |idx1|
    name1 = winter_students[idx1]
    j = idx1 + 1
    while j < winter_students.length
      name2 = winter_students[j]
      combos << [name1, name2]
      j += 1
    end
  end
  combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  bio_hash = Hash.new(0)

  specimens.each {|animal| bio_hash[animal] += 1}

  smallest_population_size = bio_hash.values.min
  largest_population_size = bio_hash.values.max

  (bio_hash.length**2) * (smallest_population_size / largest_population_size)


end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_count = character_count(vandalized_sign)

  vandalized_count.all? do |character, count|
    normal_count[character.downcase] >= count
  end
end

def character_count(str)
  count = Hash.new(0)
  new_str = str.delete(" ")

  new_str.each_char { |char| count[char.downcase] += 1 }

  count
end















#
